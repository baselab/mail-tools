#!/bin/bash

mta="mta.corp.firm.com"
port=25
helo="localhost"
from="${1}"
to="${2}"
subject="Postfix Test"

{
	sleep 0.5
    	echo "EHLO ${helo}"
	sleep 0.5
    	echo "MAIL from: ${from}"
	sleep 0.5
    	echo "RCPT to: ${to}"
	sleep 0.5
    	echo "DATA"
	sleep 0.5
    	cat <<EOF
Subject: ${subject}
Dear System Administrator,

First of all, thank you for your interest in the Postfix project.
What is Postfix? It is Wietse Venema's mail server that started
life at IBM research as an alternative to the widely-used Sendmail
program. Now at Google, Wietse continues to support Postfix.
Postfix attempts to be fast, easy to administer, and secure.
The outside has a definite Sendmail-ish flavor, but the inside is
completely different.

Best regards

The Postfix Team
EOF
	sleep 0.5
    	echo .
	sleep 0.5
    	echo "QUIT"
} | telnet ${mta} ${port}
