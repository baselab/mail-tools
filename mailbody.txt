
Dear System Administrator,

First of all, thank you for your interest in the Postfix project.
What is Postfix? It is Wietse Venema's mail server that started
life at IBM research as an alternative to the widely-used Sendmail
program. Now at Google, Wietse continues to support Postfix.
Postfix attempts to be fast, easy to administer, and secure.
The outside has a definite Sendmail-ish flavor, but the inside is
completely different.

Best regards

The Postfix Team
