#!/bin/bash

USER=$(id -nu)
mail_dir="/home/${USER}/Maildir"
mail_new=$(find ${mail_dir}/new -type d -empty -exec echo 1 \;)
mail_cur=$(find ${mail_dir}/cur -type d -empty -exec echo 1 \;)
mail_tot=$((mail_new + mail_cur))
mail_CMD=$(which mail)

if [[ "${mail_tot}" != 2 ]] ; then
    export XDG_RUNTIME_DIR="/tmp/runtime-${USER}"
    mkdir -p ${XDG_RUNTIME_DIR}
    chmod -R 700 ${XDG_RUNTIME_DIR}
    export DBUS_SESSION_BUS_ADDRESS=$( ps -u ${USER} e \
        | grep -Eo 'dbus-daemon.*ADDRESS=unix:abstract=/tmp/dbus-[A-Za-z0-9]{10}' \
        | tail -c35 2>/dev/null )
    export DISPLAY=:0

    title="Messages!"
#   message="You have the following messages:\n$(${mail_CMD} -H -f ${mail_dir}/ \
#	| awk '{print "from:",$3,"::",$9}')"
    message="You have the following messages:\n$( ${mail_CMD} -H -f ${mail_dir}/ \
	| awk '{printf $3": "; for(i=9;i<=NF;i++){printf "%s ", $i}; printf "\n"}' )"

#   /usr/bin/notify-send "${title}" "${message}"
    /usr/bin/kdialog --title "${title}" --passivepopup "${message}"
fi
