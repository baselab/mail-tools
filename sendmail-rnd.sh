#!/bin/bash

while true ; do
	usersfile="mailto.conf"
	body="mailbody.txt"
	readarray -t users < ${usersfile}
	usersnum=$(grep -c '' ${usersfile})
	usertopindex=$((usersnum - 1))

#	indexfrom=$(shuf -i 0-${usertopindex} -n 1)
#	userfrom="${users[${indexfrom}]%@*}"
	userfrom="user1.postfix"

	indexto=$(shuf -i 0-${usertopindex} -n 1)
	userto="${users[${indexto}]}"

	su ${userfrom} -s /bin/sh -c "mutt -s 'Postfix Test' ${userto} < ${body}"

	delayrange=${1}
	if [[ -z "${delayrange}" ]] ; then
		delayrange="1-300"
	fi
	delaymin=${delayrange//-*/}
	delaymax=${delayrange//*-/}
	sleep $(shuf -i ${delaymin}-${delaymax} -n 1)
done
